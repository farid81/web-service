package ca.farid.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.farid.entity.Address;
import ca.farid.entity.Customer;
import ca.farid.entity.Gender;
import ca.farid.entity.Payment;
import ca.farid.exception.CustomerErrorResponse;
import ca.farid.exception.CustomerNotFoundException;

@RestController
@RequestMapping("/api")
public class CustomerRestController {

	private List<Customer> theCustomers;
	
	// We want to create the list of customers only one time
	// in previous approach by each request every time new array of students was built
	// with @PostConstruct student initialization will happened only one time
	@PostConstruct
	public void loadData() {

		theCustomers = new ArrayList<>();
		
		Address add1 = new Address("520", "Adalbert", "Montreal", "Quebec", "Canada", "H4W 2G4");
		Payment pay1 = new Payment("Visa", "121212");
		Customer c1 = new Customer("Farid", "Eslambolchi", "22-2-1990", "farid@gmail.com", "514-445-5555", Gender.Male, pay1, add1);
		
		Address add2 = new Address("520", "Adalbert", "Sherbrook", "Quebec", "Canada", "H4W 2G4");
		Payment pay2 = new Payment("Master", "444444");
		Customer c2 = new Customer("Elahe", "Abbasi", "10-2-1992", "eli@gmail.com", "514-333-9999", Gender.Female, pay2, add2);
		
		theCustomers.add(c1);
		theCustomers.add(c2);
		
	}
	
	// define end point for "/students" - return list of students
	@GetMapping("/customers")
	public List<Customer> getCustomers() {
		return theCustomers;
	}
	
	
	@GetMapping("/customers/{customerId}")
	public Customer getCustomer(@PathVariable int customerId) {
		
		//.....................................
		// Step 3
		if( (customerId >= theCustomers.size()) || (customerId  < 0 ) ){
			throw new CustomerNotFoundException("customer id not found -  " + customerId);
		}
		//.....................................
		
		return theCustomers.get(customerId);
	}
	
	//customer by city
	@GetMapping("/customers/city/{cityName}")
	public List<Customer> getCustomersByCity(@PathVariable String cityName) {
		if (theCustomers.size() == 0) {
			throw new CustomerNotFoundException("The customers list is empty");
		}
		List<Customer> customersByCity = new ArrayList<Customer>();
		for (Customer c : theCustomers) {
			if (c.getAddress().getCity().equalsIgnoreCase(cityName)) {
				customersByCity.add(c);
			}
		}
		if (customersByCity.size() == 0) {
			throw new CustomerNotFoundException("No customers found: " + cityName);
		}
		return customersByCity;
	}
	
	//Sort Customers by family 
	@GetMapping("/customers/sorted/family")
	public List<Customer> getCustomersSortedByFamily() {
		if (theCustomers.size() == 0) {
			throw new CustomerNotFoundException("The customers list is empty");
		}
		Collections.sort(theCustomers, new Comparator<Customer>() {
			@Override
			public int compare(Customer a, Customer b) {
				return a.compareTo(b);
			}
		});
		return theCustomers;
	}
	
	//Find customers by family
	@GetMapping("/customers/find/{family}")
	public List<Customer> findCustomerByFamily(@PathVariable String family) {
		if (theCustomers.size() == 0) {
			throw new CustomerNotFoundException("The customers list is empty");
		}
		List<Customer> customerByFamily = new ArrayList<Customer>();
		for (Customer c : theCustomers) {
			if (c.getFamily().equalsIgnoreCase(family)) {
				customerByFamily.add(c);
			}
		}
		if (customerByFamily.size() == 0) {
			throw new CustomerNotFoundException("No customers found: " + family);
		}
		return customerByFamily;
	}
	
}
